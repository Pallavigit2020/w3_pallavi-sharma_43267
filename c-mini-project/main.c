#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"



void tester()
{
    user_d u;
    user_data(&u);
    user_display(&u);
	

}

void sign_in() {
	char email[30], password[10];
	user_d u;
	int invalid_user = 0;
	
	printf("email: ");
	scanf("%s", email);
	printf("password: ");
	scanf("%s", password);
	
	if(user_find_by_email(&u, email) == 1) {
		
		if(strcmp(password, u.password) == 0) {
			
			if(strcmp(email, EMAIL_OWNER) == 0)
				strcpy(u.role, ROLE_OWNER);

			
			if(strcmp(u.role, ROLE_OWNER) == 0)
				owner_area(&u);
			else if(strcmp(u.role, ROLE_LIBRARIAN) == 0)
				librarian_area(&u);
			else if(strcmp(u.role, ROLE_MEMBER) == 0)
				member_area(&u);
			else
				invalid_user = 1;
		}
		else
			invalid_user = 1;
	}
	else
		invalid_user = 1;

	if(invalid_user)
		printf("Invalid email, password or role.\n");
}

void sign_up() {
	
	user_d u;
	user_data(&u);
	
	user_add(&u);
}


int main()
{
   // tester();
   

   int choice;
	do {
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: 
			sign_in();
			break;
		case 2:	
			sign_up();	
			break;
		}
	}while(choice != 0);

    return 0;

}