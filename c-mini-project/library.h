
#define _LIBRARY_H

#include "date.h"


#define ROLE_OWNER "owner"
#define ROLE_LIBRARIAN "librarian"
#define ROLE_MEMBER "member"

#define STATUS_AVAIL "available"
#define STATUS_ISSUED "issued"

#define PAYMENT_FEES "fee"
#define PAYMENT_FINE "fine"

#define FINE_PER_DAY			5
#define BOOK_RETURN_DAYS		7
#define MEMBERSHIP_MONTH_DAYS	30

#define EMAIL_OWNER		"test@sunbeam.com"

#define USER_DB		    "users.db"
#define BOOK_DB		    "books.db"
#define BOOKCOPY_DB	    "bookcopies.db"
#define ISSUERECORD_DB  "issuerecord.db"
#define PAYMENT_DB      "payrecord.db"


typedef struct user {

    int userid;
    char name[50];
    char email[30];
    char phone[12];
    char password[15];
    char role[20];

} user_d;

typedef struct book {

    int bookid;
    char name[50];
    char author[50];
    char subject[50];
    double price;
    char isbn[20];


}book_d;

typedef struct bookcopy {

    int id;
    int bookid;
    int rack;
    char status[30];

}bookcopy_d;

typedef struct issuerecord {

    int id;
    int copyid;
    int userid;
    date_t issue_date;
	date_t return_duedate;
	date_t return_date;

    double fine;

}issuerecord_d;

typedef struct payment {

    int id;
    int userid;
    double amount;
    char payment_type[20];
    date_t tx_time;
	date_t next_pay_duedate;


}payment_d;

//user functions
void user_data(user_d *u);
void user_display(user_d *u);
void edit_user_by_id(int id, char role[]);
void change_password(int id, char password[]);

//Book functions
void book_data(book_d *b);
void book_display(book_d *b);
void bookcopy_accept(bookcopy_d *c);
void bookcopy_display(bookcopy_d *c);
void change_rack();



//owner function
void owner_area(user_d *u);
void appoint_librarian();

//librarian functions
void librarian_area(user_d *u);
void add_book();
void add_member();
void add_copy();
void bookcopy_accept(bookcopy_d *c);
void bookcopy_display(bookcopy_d *c);
void bookcopy_checkavail_details();
void book_edit_by_id();
void bookcopy_issue();
void bookcopy_return(); 
void display_issued_bookcopies(int member_id);
void bookcopy_changestatus(int bookcopy_id, char status[]);

// issue record function
void issuerecord_accept(issuerecord_d *r);
void issuerecord_display(issuerecord_d *r);

//member functions
void member_area(user_d *u);


//common functions

void sign_in();
void sign_up();
void user_add(user_d *u);
int user_find_by_email(user_d *u, char email[]);
int get_next_user_id();
int get_next_book_id();
int get_next_bookcopy_id(); 
void book_find_by_name(char name[]);
int get_next_issuerecord_id();

//payment functions

void payment_accept(payment_d *p);
void payment_display(payment_d *p); 
int get_next_payment_id();
int is_paid_member(int memberid);
void payment_history(int memberid);
void fees_payment_add();
void fine_payment_add(int memberid, double fine_amount);
