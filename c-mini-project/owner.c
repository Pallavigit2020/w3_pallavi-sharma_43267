#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_area(user_d *u) {
	int choice;
	char newpass[20];
	char name[20];
	do {
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:	
                 appoint_librarian();			
				break;
			case 2:				
				//edit_user_by_id(u->userid, u->role);
				break;
			case 3:
				printf("Enter new password:");
				scanf("%s",newpass);
				change_password(u->userid, newpass);
				break;
			case 4:
				break;
			case 5:
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
				break;
			case 6:
				break;
		}
	}while (choice != 0);	
}

void appoint_librarian() {
	
	user_d u;
	user_data(&u);
	
	strcpy(u.role, ROLE_LIBRARIAN);
	
	user_add(&u);
}

