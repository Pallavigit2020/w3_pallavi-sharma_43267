#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"


void member_area(user_d *u) {
	int choice;
	char name[80];
	char newpass[20];
	
	do {
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:	
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
			
				break;
			case 2:
				
				edit_user_by_id(u->userid, u->role);

				break;
			case 3:
				printf("Enter new password:");
				scanf("%s",newpass);
				change_password(u->userid, newpass);
				break;
			case 4:
			bookcopy_checkavail_details();
				break;
		}
	}while (choice != 0);	
}
