#include <stdio.h>
#include <string.h>
#include "library.h"


void user_data(user_d *u)
{
	
    u->userid = get_next_user_id();

    printf("Enter Name :");
    scanf("%s", &u ->name);

    printf("Email Id :");
    scanf("%s", &u ->email);

    printf("Phone no  :");
    scanf("%s", &u ->phone);

    printf("Password :");
    scanf("%s", &u ->password);

    strcpy(u->role, ROLE_MEMBER);
    
}

void user_display(user_d *u)
{
    printf("%d,%s,%s,%s,%s", u->userid, u->name, u->email, u->phone, u->role);
}

void user_add(user_d *u) {

	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("failed to open users file");
		return;
	}

	
	fwrite(u, sizeof(user_d), 1, fp);
	printf("user added into file.\n");
	
	
	fclose(fp);
}

int user_find_by_email(user_d *u, char email[]) {
	FILE *fp;
	int found = 0;
	
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open users file");
		return found;
	}
	
	while(fread(u, sizeof(user_d), 1, fp) > 0) {
		
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	
	fclose(fp);
	
	return found;
}

int get_next_user_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(user_d);
	user_d u;
	
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	fseek(fp, -size, SEEK_END);
	
	if(fread(&u, size, 1, fp) > 0)
	
		max = u.userid;
	
	fclose(fp);

	return max + 1;
}

int get_next_book_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(book_d);
	book_d b;
	
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	fseek(fp, -size, SEEK_END);
	
	if(fread(&b, size, 1, fp) > 0)
	
		max = b.bookid;
	
	fclose(fp);

	return max + 1;
}

void book_data(book_d *b) {
	
	 b->bookid = get_next_book_id();

	printf("name: ");
	scanf("%s", b->name);
	printf("author: ");
	scanf("%s", b->author);
	printf("subject: ");
	scanf("%s", b->subject);
	printf("price: ");
	scanf("%lf", &b->price);
	printf("isbn: ");
	scanf("%s", b->isbn);
}

void book_display(book_d *b) {
	printf("%d, %s, %s, %s, %.2lf, %s\n", b->bookid, b->name, b->author, b->subject, b->price, b->isbn);
}

void bookcopy_accept(bookcopy_d *c) {
	
	c->id = get_next_bookcopy_id();
	printf("book id: ");
	scanf("%d", &c->bookid);
	printf("rack: ");
	scanf("%d", &c->rack);
	strcpy(c->status, STATUS_AVAIL);
}

void bookcopy_display(bookcopy_d *c) {
	printf("%d, %d, %d, %s\n", c->id, c->bookid, c->rack, c->status);
}

int get_next_bookcopy_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_d);
	bookcopy_d u;
	
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	fseek(fp, -size, SEEK_END);
	
	if(fread(&u, size, 1, fp) > 0)
		
		max = u.id;
	
	fclose(fp);
	
	return max + 1;
}

void book_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	book_d b;
	// open the file for reading the data
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&b, sizeof(book_d), 1, fp) > 0) {
		// if book name is matching partially, found 1
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}

void issuerecord_accept(issuerecord_d *r) {
	
	r->id = get_next_issuerecord_id();

	printf("copy id: ");
	scanf("%d", &r->copyid);
	printf("member id: ");
	scanf("%d", &r->userid);
	printf("issue ");
	date_accept(&r->issue_date);
	
	r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);
	memset(&r->return_date, 0, sizeof(date_t));
	r->fine = 0.0;
}

void issuerecord_display(issuerecord_d *r) {
	printf("issue record: %d, copy: %d, member: %d, fine: %.2lf\n", r->id, r->copyid, r->userid, r->fine);
	printf("issue ");
	date_print(&r->issue_date);
	printf("return due ");
	date_print(&r->return_duedate);
	printf("return ");
	date_print(&r->return_date);
}

int get_next_issuerecord_id(){
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_d);
	issuerecord_d u;
	
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	fseek(fp, -size, SEEK_END);
	
	if(fread(&u, size, 1, fp) > 0)
		
		max = u.id;
	
	fclose(fp);
	
	return max + 1;
}

void edit_user_by_id(int id, char role[])
{
	int  found = 0;
	FILE *fp;
	user_d u;
			
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open user file");
		
	}
	
	while(fread(&u, sizeof(user_d), 1, fp) > 0) {
		if(id == u.userid) {
			found = 1;
			break;
		}
	}
	
	if(found) {
		
		long size = sizeof(user_d);
		
		user_data(&u);
		u.userid = id;
		strcpy(u.role, role);

		fseek(fp, -size, SEEK_CUR);
		
		fwrite(&u, size, 1, fp);	
		
		printf("\n Member profile updated.\n");
		fclose(fp);
	}
	else
	{ 
		printf("Member not found.\n");
	
	fclose(fp);
	}
}

void payment_accept(payment_d *p) {

	p->id = get_next_payment_id();

	printf("member id: ");
	scanf("%d", &p->userid);

	strcpy(p->payment_type, PAYMENT_FEES);
	printf("amount: ");
	scanf("%lf", &p->amount);
	p->tx_time = date_current();
	
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	
}

void payment_display(payment_d *p) 
{
	printf("payment Id: %d, member Id: %d, Payment Type: %s, amount: %.2lf\n", p->id, p->userid, p->payment_type, p->amount);
	printf("payment ");
	date_print(&p->tx_time);
	printf("payment due");
	date_print(&p->next_pay_duedate);
}

int get_next_payment_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_d);
	payment_d u;
	
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	fseek(fp, -size, SEEK_END);
	
	if(fread(&u, size, 1, fp) > 0)
		
		max = u.id;
	
	fclose(fp);

	return max + 1;
}

void change_password(int id , char password[])
{
	user_d u;
	FILE *fp;
	long size = sizeof(user_d);
	
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open user file");
		return;
	}

	
	while(fread(&u, sizeof(user_d), 1, fp) > 0) {
		
		if(id == u.userid) {
		
			strcpy(u.password, password);
			
			fseek(fp, -size, SEEK_CUR);
			
			fwrite(&u, sizeof(user_d), 1, fp);
			printf("password changed successfully");
			break;
		}
	}
	
	
	fclose(fp);

}
