#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"


void librarian_area(user_d *u) {
	int choice;
	char name[80];
	char newpass[20];
	int memberid;
	do {
		printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:		
				add_member();	
				break;
			case 2:
				
				edit_user_by_id(u->userid, u->role);
				break;
			case 3:
				printf("Enter new password:");
				scanf("%s",newpass);
				change_password(u->userid, newpass);
				break;
			case 4:
				add_book();
				break;
			case 5: 
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);

				break;
			case 6: 
				book_edit_by_id();
                break;
			case 7:
				bookcopy_checkavail_details();
				break;
			case 8: 
				add_copy();
				break;
			case 9:
			
				change_rack();
				break;
			case 10:
				bookcopy_issue();
				break;
			case 11:
				bookcopy_return();
				break;
			case 12:
				fees_payment_add();
				break;
			case 13:

				printf("enter member id of the member: ");
				scanf("%d", &memberid);
				payment_history(memberid);

				break;
		}
	}while (choice != 0);		
}

void add_member() {

	user_d u;
	user_data(&u);
	
	user_add(&u);
}



void add_book() {
	FILE *fp;
	
	book_d b;
	book_data(&b);
	b.bookid = get_next_book_id();
	
	fp = fopen(BOOK_DB, "ab");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	
	fwrite(&b, sizeof(book_d), 1, fp);
	printf("book added in file.\n");
	
	fclose(fp);
}

void add_copy() {
	FILE *fp;
	
	bookcopy_d b;
	bookcopy_accept(&b);
	b.id = get_next_bookcopy_id();
	
	fp = fopen(BOOKCOPY_DB, "ab");
	if(fp == NULL) {
		perror("cannot open book copies file");
		exit(1);
	}
	
	fwrite(&b, sizeof(bookcopy_d), 1, fp);
	printf("book copy added in file.\n");
	
	fclose(fp);
}


void bookcopy_checkavail_details() {
	int book_id;
	FILE *fp;
	bookcopy_d bc;
	int count = 0;
	
	printf("enter the book id: ");
	scanf("%d", &book_id);
	
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}

	
	while(fread(&bc, sizeof(bookcopy_d), 1, fp) > 0) {
	
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
			bookcopy_display(&bc);
			count++;
		}
	}
	
	fclose(fp);

	if(count == 0)
		printf("no copies availables.\n");
}

void book_edit_by_id() {
	int id, found = 0;
	FILE *fp;
	book_d b;

	printf("enter book id: ");
	scanf("%d", &id);
	
	fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	
	while(fread(&b, sizeof(book_d), 1, fp) > 0) {
		if(id == b.bookid) {
			found = 1;
			break;
		}
	}
	
	if(found) {
		
		long size = sizeof(book_d);
		book_d nb;
		book_data(&nb);
		nb.bookid = b.bookid;
		
		fseek(fp, -size, SEEK_CUR);
		
		fwrite(&nb, size, 1, fp);
		printf("book updated.\n");
		
	}
	else 
		printf("Book not found.\n");
	
	fclose(fp);
}

void bookcopy_issue() {
	issuerecord_d rec;
	FILE *fp;
	
	issuerecord_accept(&rec);
	
	if(!is_paid_member(rec.userid)) {
		printf("member is not paid.\n");
		return;
	}

	rec.id = get_next_issuerecord_id();
	
	fp = fopen(ISSUERECORD_DB, "ab");
	if(fp == NULL) {
		perror("issuerecord file cannot be opened");
		exit(1);
	}
	
	fwrite(&rec, sizeof(issuerecord_d), 1, fp);
	
	fclose(fp);

	bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}

void bookcopy_return() {
	int member_id, record_id;
	FILE *fp;
	issuerecord_d rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_d);
	
	printf("enter member id: ");
	scanf("%d", &member_id);
	
	display_issued_bookcopies(member_id);
	
	printf("enter issue record id (to return): ");
	scanf("%d", &record_id);
	
	fp = fopen(ISSUERECORD_DB, "rb+");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}

	while(fread(&rec, sizeof(issuerecord_d), 1, fp) > 0) {
		
		if(record_id == rec.id) {
			found = 1;
			
			rec.return_date = date_current();
			
			
			diff_days = date_cmp(rec.return_date, rec.return_duedate);
			
			if(diff_days > 0)
				rec.fine = diff_days * FINE_PER_DAY;
				fine_payment_add(rec.userid, rec.fine);
				printf("fine amount Rs. %.2lf/- is applied.\n", rec.fine);

			break;
		}
	}
	
	if(found) {

		
		fseek(fp, -size, SEEK_CUR);
		
		fwrite(&rec, sizeof(issuerecord_d), 1, fp);
		
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);
		
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	}
	
	
	fclose(fp);
}


void display_issued_bookcopies(int member_id) {
	FILE *fp;
	issuerecord_d rec;
	
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}

	
	while(fread(&rec, sizeof(issuerecord_d), 1, fp) > 0) {
		
		if(rec.userid == member_id && rec.return_date.day == 0)
			issuerecord_display(&rec);
	}
	
	fclose(fp);
}

void bookcopy_changestatus(int bookcopy_id, char status[]) {
	bookcopy_d bc;
	FILE *fp;
	long size = sizeof(bookcopy_d);
	
	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book copies file");
		return;
	}

	
	while(fread(&bc, sizeof(bookcopy_d), 1, fp) > 0) {
		
		if(bookcopy_id == bc.id) {
		
			strcpy(bc.status, status);
			
			fseek(fp, -size, SEEK_CUR);
			
			fwrite(&bc, sizeof(bookcopy_d), 1, fp);
			break;
		}
	}
	
	
	fclose(fp);
}

void fees_payment_add() {
	FILE *fp;
	
	payment_d pay;
	payment_accept(&pay);
	pay.id = get_next_payment_id();
	
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}
	
	fwrite(&pay, sizeof(payment_d), 1, fp);
	
	fclose(fp);
}

void payment_history(int memberid) {
	FILE *fp;
	payment_d pay;
	
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return;
	}
	
	while(fread(&pay, sizeof(payment_d), 1, fp) > 0) {
		if(pay.userid == memberid)
			payment_display(&pay);
			
			
	}
	
	fclose(fp);
}

int is_paid_member(int memberid) {
	date_t now = date_current();
	FILE *fp;
	payment_d pay;
	int paid = 0;
	// open file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return 0;
	}
	// read payment one by one till eof
	while(fread(&pay, sizeof(payment_d), 1, fp) > 0) {
		if(pay.userid == memberid && pay.next_pay_duedate.day != 0 && date_cmp(now, pay.next_pay_duedate) < 0) {
			paid = 1;
			break;
		}
	}
		
	fclose(fp);
	return paid;
}

void fine_payment_add(int memberid, double fine_amount) {
	FILE *fp;
	
	payment_d pay;
	pay.id = get_next_payment_id();
	pay.userid = memberid;
	pay.amount = fine_amount;
	strcpy(pay.payment_type, PAYMENT_FINE);
	pay.tx_time = date_current();
	memset(&pay.next_pay_duedate, 0, sizeof(date_t));
	
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}
	
	fwrite(&pay, sizeof(payment_d), 1, fp);
	fclose(fp);
}

void change_rack()
{
	int id, nrack;
	int found =0;
	bookcopy_d b;
	FILE *fp;

			
	printf("Enter copy id:\n");
	scanf("%d",&id);
	printf("Enter new rack no:");
	scanf("%d",&nrack);

	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book copies file");
		return;
	}


	while(fread(&b, sizeof(bookcopy_d), 1, fp) > 0) {
		if(id == b.id) {
			found = 1;
			break;
		}
	}
	
	if(found) {
		
		long size = sizeof(bookcopy_d);
				
		b.id = id;
		b.rack = nrack;
		
		fseek(fp, -size, SEEK_CUR);
		
		fwrite(&b, size, 1, fp);
	
		printf("book copy updated.\n");
		fclose(fp);
	}
	else 
		printf("Book not found.\n");
	
	fclose(fp);
	
}
