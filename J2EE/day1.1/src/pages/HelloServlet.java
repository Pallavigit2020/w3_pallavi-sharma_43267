package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hi")//implicit element of the @WebServlet anno : urlPattterns / value
// key : /hi
//value : Fully qualified servlet cls name : pages.HelloServlet
//Above entry is added by WC ,  in the WC created map (used for mapping incoming request to a sevlet)
public class HelloServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//set resp content type : text/html
		//API : HttpServletResponse(inherited from ServletResponse) 
		//public void setContentType(String contType)
		resp.setContentType("text/html");
		//get a PrintWriter for sending text resp  from server ----> to the client
		//Method of HttpServletResponse(inherited from ServletResponse)
		//public PrintWriter getWriter() throws IOException
		try(PrintWriter pw=resp.getWriter())
		{
			pw.print("<h5> Welcome 2 servlets !!!!! @ "+LocalDateTime.now()+"</h5>");
		}
	}
	

}
