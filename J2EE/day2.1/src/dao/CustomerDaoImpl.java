package dao;

import java.sql.*;
import static utils.DBUtils.getDBConnection;
import pojo.Customer;


public class CustomerDaoImpl implements ICustomerDao {
	//state
	private Connection cn;
	private PreparedStatement pst1;
	//def ctor
	public CustomerDaoImpl() throws ClassNotFoundException,SQLException{
		// get fixed db connection
		cn=getDBConnection();
		//pst1 : to hold pre parsed n pre compiled stmt
		pst1=cn.prepareStatement("select * from my_customers where email=? and password=?");
		System.out.println("customer dao created !");
		
	}

	@Override
	public Customer authenticateCustomer(String email, String password) throws SQLException {
		// set IN params
		pst1.setString(1,email);
		pst1.setNString(2, password);
		//exec query
		try(ResultSet rst=pst1.executeQuery())
		{
			if(rst.next())
				return new Customer(rst.getInt(1), email, password, rst.getDouble(4), rst.getDate(5));
		}
		return null;
	}
	public void cleanUp() throws SQLException
	{
		if(pst1 != null)
			pst1.close();
		if(cn != null)
			cn.close();
		System.out.println("customer dao cleaned up!");
			
	}

}