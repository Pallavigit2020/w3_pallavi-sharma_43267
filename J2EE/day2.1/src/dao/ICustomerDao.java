package dao;

import pojo.Customer;
import java.sql.SQLException;

public interface ICustomerDao {

	Customer authenticateCustomer(String email,String password) throws SQLException;
}
