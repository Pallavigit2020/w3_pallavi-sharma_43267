package utils;
import java.sql.*;

public interface DBUtils {
//add a static method to return FIXED DB connection
	static Connection getDBConnection() throws ClassNotFoundException,SQLException
	{
		//load JDBC driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/j2ee?useSSL=false", "root", "manager");
	}
}

            