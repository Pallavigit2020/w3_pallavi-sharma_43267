package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CustomerDaoImpl;
import pojo.Customer;

/**
 * Servlet implementation class AuthenticationServlet
 */
@WebServlet(value="/authenticate",loadOnStartup = 1)
public class AuthenticationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CustomerDaoImpl customerDao;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	//Overrding form of the method CAN'T add any NEW or BROADER CHECKED excs
	//Centralized error handling in servlets 
	@Override
	public void init() throws ServletException {
		try {
		// create dao instance
		customerDao=new CustomerDaoImpl();
		} catch (Exception e) {
			throw new ServletException("err in init : "+getClass().getName(),e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		//clean up
		try {
			customerDao.cleanUp();
		} catch (SQLException e) {
		//	System.out.println("err in destroy : "+getClass().getName()+" exc "+e);
			//how to re throw this exc to the caller : WC ?
			throw new RuntimeException("err in destroy : "+getClass().getName(), e);//not essential
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set response cont type
		response.setContentType("text/html");
		//get PW to send text resp to the clnt
		try(PrintWriter pw=response.getWriter())
		{
			//get request data
			String email = request.getParameter("em");
			String pwd = request.getParameter("pass");
			//invoke dao method
			Customer customer = customerDao.authenticateCustomer(email,pwd);
			
			if(customer==null)
			{
				pw.print("<h5>Invalid Login please retry<a href= 'login.html'>Retry</a></h5>");
			}
			else
			{
				//pw.print("<h5>Login Successful !</h5>");
				//pw.print("<h5>your details"+ customer +"</h5>");
				
				response.sendRedirect("category");
			}
		} catch (SQLException e) {
			// re throw the exception
			throw new ServletException("err in do-post:" +getClass().getName(),e);
			
		}
	}

}
