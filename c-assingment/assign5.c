#include <stdio.h>
#include <string.h>

#include "date.c"

typedef struct employee
{
    int emp_id;
    char name[50];
    char address[100];
    int salary;
    date_t dateof_birth;
    date_t dateof_join;
}employ_d;

void employee_accept(employ_d *e)
{
   
    printf("Enter employee Id :");
    scanf("%d",&e->emp_id);
    printf("Enter employee Name :");
    scanf("%s",&e->name);
    printf("Enter  Address :");
    scanf("%s",&e->address);
    printf("Enter salary :");
    scanf("%d",&e->salary);
    printf("Enter Birth");
    date_accept(&e->dateof_birth);
    printf("Enter Joining");
    date_accept(&e->dateof_join);
}

void employee_display(employ_d *e)
{
   // printf("%d,%s,%s,%d", e->emp_id,e->name,e->address,e->salary);
   // date_print(&e->dateof_birth);
   // date_print(&e->dateof_join);
   int diff =  date_cmp(e->dateof_join, e->dateof_birth);
   int age = diff / 365;
   printf("Age as on joining : %d", age);
   printf("years\n");
   date_t today = date_current();
   int diff2 = date_cmp(today, e->dateof_join);
   printf("Experience as of today : %d", diff2);
   printf("days");
}

int main()
{
    employ_d e;
   employee_accept(&e);
   employee_display(&e);
   

    return 0;
}