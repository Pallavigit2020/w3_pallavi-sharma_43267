#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int string_compare(char *x, char *y)
{
     int flag = 0; 
  
      while (*x != '\0' || *y != '\0') { 
        if (*x == *y) { 
            x++; 
            y++; 
        } 
          else if ((*x == '\0' && *y != '\0') 
                 || (*x != '\0' && *y == '\0') 
                 || *x != *y) { 
            flag = 1; 
            printf("\n Different Strings\n"); 
            break; 
        } 
    } 
  
    if (flag == 0) { 
        printf("\n Same Strings\n"); 
    } 

}

int string_concat(char str1[], char str2[])
{
    int i,j;

    for(i=0; str1[i]!='\0'; ++i); 
 
     for(j=0; str2[j]!='\0'; ++j, ++i)
   {
      str1[i]=str2[j];
   }
     str1[i]='\0';
   printf("\nOutput: %s",str1);
   
}

int string_copy(char str1[])
{
    int i;
    char str3[100];

    for (i = 0; str1[i] != '\0'; ++i) {
        str3[i] = str1[i];
    }

    str3[i] = '\0';
    printf("\n Output: %s", str3);
    return 0 ;
}

int string_rev(char str1[])
{
    char temp;
   int i, j = 0;
 
   i = 0;
   j = strlen(str1) - 1;
 
   while (i < j) {
      temp = str1[i];
      str1[i] = str1[j];
      str1[j] = temp;
      i++;
      j--;
   }
 
   printf("%s", str1);
   return (0);
}

int main()
{
    char str1[100];
    char str2[100];

    printf("Enter a string:");
    gets(str1);
    printf("Enter second string:");
    gets(str2);

    printf("Comparing both the strings:\n");
    string_compare(str1,str2);
   
    printf("Copy the first string :\n");
    string_copy(str1);
    
     printf("\n Concatinating both the strings:\n");
    string_concat(str1 , str2);

    printf("\n Reversing the string:\n");
    string_rev(str1);
    

    return 0;


}