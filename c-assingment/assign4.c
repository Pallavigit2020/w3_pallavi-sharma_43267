#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct book {
	int id;
	char name[40];
	int price;
}book_t;

void merge_sort(book_t arr[], int left, int right) {
	int mid, i, j, k, n;
	book_t *temp;
	
	if(left == right || left > right)
		return;
	
	mid = (left + right) / 2;
	
	merge_sort(arr, left, mid);
	
	merge_sort(arr, mid+1, right);
	
	n = right - left + 1;
	temp = (book_t*) malloc(n * sizeof(book_t));
	
	i = left;
	j = mid+1;
	k = 0;
	while(i <= mid && j <= right) {
	
		if(arr[i].price > arr[j].price) {
			temp[k] = arr[i];
			i++;
			k++;
		}
		else {
			temp[k] = arr[j];
			j++;
			k++;
		}
	}
	while(i <= mid) {
		temp[k] = arr[i];
		i++;
		k++;
	}
	while(j <= right) {
		temp[k] = arr[j];
		j++;
		k++;
	}
	
	for(i=0; i<n; i++)
		arr[left + i] = temp[i];
	
	free(temp);
}
int comparator(const void* p, const void* q) 
{ 
    return strcmp(((book_t*)p)->name, 
                  ((book_t*)q)->name); 
} 


int main() {
	book_t arr[10] = {
		{7, "Brillient world of Tom gate", 734}, 
		{1, "Excellent Excuses", 623}, 
		{5, "Everything Amazing", 532},
		{3, "Genious Ideas", 325},
		{4, "Absolutely Fantanstic", 587},
		{8, "Mortals of Meluha", 973},
		{9, "Old School", 534},
		{10, "Spectacular School Trip", 238},
		{6, "What Monster", 592},
		{2, "The secret", 351}
	};
	int i, len = 10 ,j;
	merge_sort(arr, 0, len-1);
    printf("Books list After Merge sort-----\n");
	for(i=0; i<len; i++)
		printf("%d, %s, %d\n", arr[i].id, arr[i].name, arr[i].price);
	printf("\n");
     qsort(arr, len-1, sizeof(book_t), comparator); 
     printf("Books list After Qsort-----\n");
    for(j=0; j<len; j++)
		printf("%d, %s, %d\n", arr[j].id, arr[j].name, arr[j].price);
	printf("\n");

    return 0;
}

