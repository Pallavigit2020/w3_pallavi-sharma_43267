// c program to spilt a string into words.
// Author: Pallavi Sharma
// eDMC sep 2020

#include <stdio.h>
#include <string.h>

int main () {

   char str[100]; 
   
   printf("Enter a string : ");
   gets(str);
   const char s[2] = ",";
   char *token;
   
   /* get the first token */
   token = strtok(str, s);
   
   /* walk through other tokens */
   while( token != NULL ) {
      printf( " %s\n", token );
    
      token = strtok(NULL, s);
   }
   
   return(0);
}